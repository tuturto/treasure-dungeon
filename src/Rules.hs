{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use lambda-case" #-}

module Rules
  ( dice, rollNewCharacter, startingDeck, startingItem, flipCard, startGame
  , storageRoomItem, checkForEnd, isDiscardable, roomItems, attack
  , runAway, jumpOverChasm, ropeOverChasm, goAroundChasm
  , breakDoor, lockPickDoor, goAroundDoor, usableItem, useItem
  )
where

import Import
import Control.Monad.Random ( RandomGen, Rand, getRandomRs )
import qualified RIO.List as L

import Util ( shuffle )

-- | Rolls n times 6 sided dice and sums the results
dice :: (RandomGen g) => Natural -> Rand g Natural
dice n = do
    rolls <- getRandomRs (1, 6)
    let roll = sum $ take (fromIntegral n) rolls
    return $ fromInteger roll


-- | Creates a player with randomly assigned stats
rollNewCharacter :: (RandomGen g) => Rand g Player
rollNewCharacter = do
    str <- dice 3
    dex <- dice 3
    mind <- dice 3
    maxHp <- dice 4

    return $ Player
      { playerStrength  = MkStrength str
      , playerDexterity = MkDexterity dex
      , playerMind      = MkMind mind
      , playerHP        = MkHP maxHp
      , playerMaxHP     = MkHP maxHp
      , playerGear      = [ ]
      }


-- | Cards in the deck at the beginning of the game
startingDeck :: [ Card ]
startingDeck =
    replicate 5 EmptyPassage
    <> replicate 3 (GuardedPassage [Goblin])
    <> replicate 2 (GuardedPassage [Orc])
    <> [ GuardRoom [Orc, Orc]
       , GuardRoom [Goblin, Goblin]
       , GuardRoom [Orc, Goblin, Goblin]
       ]
    <> replicate 2 (StorageRoom [])
    <> replicate 2 (StorageRoom [Goblin])
    <> replicate 2 ArrowTrap
    <> [ Chasm
       , LockedDoor
       , TreasureChamber
       ]


-- | Is an item one that can be selected at the start of the game?
startingItem :: Item -> Bool
startingItem item =
    case item of
        Sword         -> True
        Helmet        -> True
        Armour        -> True
        Rope          -> True
        LockPick      -> True
        HealingPotion -> False
        MagicCrown    -> False


-- | Is an item one that can be found in storage room?
storageRoomItem :: Item -> Bool
storageRoomItem item =
    case item of
        Sword         -> False
        Helmet        -> False
        Armour        -> False
        Rope          -> True
        LockPick      -> True
        HealingPotion -> True
        MagicCrown    -> False


-- | There might be items in a room. Different rooms will have different
-- | selection of items available for the player to take.
roomItems :: Game -> [Item]
roomItems game@Game { gameCurrentCard = StorageRoom _ } =
    L.filter (\item -> storageRoomItem item
                        && notElem item (playerGear (gamePlayer game)))
             [minBound..]

roomItems _ = []


-- | Can item be used after current card has been resolved
usableItem :: Item -> Bool
usableItem item =
    case item of
        Sword         -> False
        Helmet        -> False
        Armour        -> False
        Rope          -> False
        LockPick      -> False
        HealingPotion -> True
        MagicCrown    -> False


-- | Shuffle deck, attach gear to the player and start the game
startGame :: (RandomGen g) => Player -> [Item] -> Rand g Game
startGame player gear = do
    deck <- shuffle startingDeck
    return $ Game
                { gamePlayer = player & playerGearL .~ gear
                , gameDeck = deck
                , gameDiscardDeck = []
                , gameCurrentCard = EmptyPassage
                }


-- | Reveal the top-most card of the deck and apply the immediate effects of it.
-- | The card previously on top is moved into discard pile or removed completely
-- | from the play, depending on the rules of the card.
flipCard :: (RandomGen g) => Game -> Rand g (Game, [ImmediateResult])
flipCard game =
    case gameDeck game of
        card : deck -> do
            (game', effects) <- applyCard game card
            return ( game' & gameDeckL .~ deck
                           & gameCurrentCardL .~ card
                           -- If card is discardable, it is placed in discard deck.
                           -- Other cards are removed from the play completely.
                           & gameDiscardDeckL %~ (\d -> case isDiscardable card of
                                                            Removable -> d
                                                            Discardable -> card : d)
                   , NewCardRevealed card : effects
                   )

        _ ->
            -- There are no cards left. This shouldn't happen, but here we are
            -- place JourneyEnd as the last card and call it a day
            return ( game & gameCurrentCardL .~ JourneyEnd
                          & gameDeckL .~ []
                   , [DeckFinished])


-- | Apply immediate effects of a card
applyCard :: (RandomGen g) => Game -> Card -> Rand g (Game, [ImmediateResult])
applyCard game JourneyEnd =
    return (game, [])

applyCard game EmptyPassage =
    return (game, [])

applyCard game (GuardedPassage _) =
    return (game, [])

applyCard game (GuardRoom _) =
    return (game, [])

applyCard game (StorageRoom _) = do
    -- only storage room items that player doesn't already have can be found here
    let items = L.filter (\item -> storageRoomItem item
                                    && notElem item (playerGear (gamePlayer game)))
                         [minBound..]
    return (game, [ItemsFound items])

applyCard game Chasm =
    return (game, [])

applyCard game LockedDoor =
    return (game, [])

applyCard game ArrowTrap = do
    roll <- dice 3
    -- in order to avoid the arrow trap, player needs to roll under their dexterity skill
    if roll < (unDexterity . playerDexterity . gamePlayer) game
        then
            return (game, [TrapAvoided])
        else do
            let player = gamePlayer game
            let newHP = if (unHP . playerHP) player >= 2
                            then
                                MkHP $ (unHP . playerHP) player - 2
                            else
                                MkHP 0
            return (game & gamePlayerL . playerHPL .~ newHP, [TrapTriggered 2])

applyCard game TreasureChamber = do
    return ( game & gamePlayerL . playerGearL %~ (MagicCrown :)
           , [ ItemsFound [MagicCrown] ])

applyCard game HiddenCorner =
    return (game, [])


-- | Has the player reached end of the game?
checkForEnd :: Game -> GameCondition
checkForEnd game
    -- finding the magic crown will end the game
    | MagicCrown `elem` playerGear (gamePlayer game) = GameEnded
    -- falling to zero HP will end the game
    | unHP (playerHP $ gamePlayer game) == 0 = GameEnded
    -- current card being end of journey ends the game
    | gameCurrentCard game == JourneyEnd = GameEnded
    | otherwise = GameRunning


-- | Should the card be placed in discard deck or removed completely after it has been resolved.
-- | Removed cards will never come back into the game after they have been resolved.
-- | Discarded cards might be shuffled back in the deck in case player needs to take a detour
-- | in order to get around an obstacle.
isDiscardable :: Card -> DiscardStatus
isDiscardable card =
    case card of
        JourneyEnd       -> Removable
        EmptyPassage     -> Discardable
        GuardedPassage _ -> Discardable
        GuardRoom _      -> Discardable
        StorageRoom _    -> Discardable
        ArrowTrap        -> Discardable
        Chasm            -> Removable
        LockedDoor       -> Removable
        TreasureChamber  -> Removable
        HiddenCorner     -> Removable


-- | Handles player attacking a monster and possible counter attacks by monsters
attack :: (RandomGen g) => Game -> Monster -> Rand g (Game, [CombatResult])
attack game monster = do
    let player = gamePlayer game
    let playerWeaponScore = sum $ weaponScore <$> playerGear player
    playerRoll <- dice 2
    monsterRoll <- dice 2

    let playerScore = playerWeaponScore + playerRoll + unStrength (playerStrength player)
    let monsterScore = monsterRoll + unStrength (monsterStrength monster)

    let (game', results) = if playerScore > monsterScore
        then
            ( game & gameCurrentCardL %~ removeMonster monster
            , [ PlayerAttackHits monster
              , MonsterDies monster
              ])
        else
            ( game
            , [ PlayerAttackMisses
              , CombatContinues
              ]
            )

    foldM counterAttack (game', results) (cardMonsters $ gameCurrentCard game')


-- | Player tries to run away from monsters.
-- | If they succeeds, up to 3 cards is returned from discard pile into deck and shuffled.
-- | If they fail, monsters get free attacks.
runAway :: (RandomGen g) => Game -> Rand g (Game, [CombatResult])
runAway game = do
    dodgeResults <- mapM (tryEvadingMonster game) (cardMonsters $ gameCurrentCard game)
    (game', attackResults) <- if any (\n -> case n of
                                                MonsterCatchesPlayer _ -> True
                                                _ -> False)
                                     dodgeResults
        then
            foldM counterAttack (game, dodgeResults) (cardMonsters $ gameCurrentCard game)
        else do
            newDeck <- shuffle $ gameDeck game <> L.take 3 (gameDiscardDeck game)
            return ( game & gameCurrentCardL .~ HiddenCorner
                          & gameDeckL .~ newDeck
                          & gameDiscardDeckL %~ L.drop 3
                   , dodgeResults)

    return (game', attackResults)


-- | Player tries to evade a monster. This tests their dexterity against
-- | monster's dexterity.
tryEvadingMonster :: (RandomGen g) => Game -> Monster -> Rand g CombatResult
tryEvadingMonster game monster = do
    let player = gamePlayer game
    playerRoll <- dice 2
    monsterRoll <- dice 2

    let playerScore = playerRoll + unDexterity (playerDexterity player)
    let monsterScore = monsterRoll + unDexterity (monsterDexterity monster)

    if playerScore > monsterScore
        then
            return $ PlayerEscapes monster
        else
            return $ MonsterCatchesPlayer monster


-- | Monster performs a counter attack. This is a test of strength between the player and
-- | the monster. Weapon and armour of the player modifies the test. Monsters never have
-- | any weapons or armour, their strength reflects their gear.
counterAttack :: (RandomGen g) => (Game, [CombatResult]) -> Monster -> Rand g (Game, [CombatResult])
counterAttack (game, results) monster = do
    let player = gamePlayer game

    if unHP (playerHP player) == 0
        then
            -- don't bother attacking if the player is already dead
            return (game, results)
        else do
            let playerArmourScore = sum $ armourScore <$> playerGear player
            playerRoll <- dice 2
            monsterRoll <- dice 2

            let playerScore = playerArmourScore + playerRoll + unStrength (playerStrength player)
            let monsterScore = monsterRoll + unStrength (monsterStrength monster)

            return $ if monsterScore > playerScore
                then
                    ( game & gamePlayerL . playerHPL %~ (\n -> MkHP $ unHP n - 1)
                    , if unHP (playerHP player) > 1
                        then
                            results <> [ MonsterAttackHits monster ]
                        else
                            results <> [ MonsterAttackHits monster
                                       , PlayerDies
                                       ]
                    )
                else
                    ( game
                    , results <> [ MonsterAttackMisses monster ]
                    )


-- | Remove given monster from a card.
removeMonster :: Monster -> Card -> Card
removeMonster monster card =
    case card of
        JourneyEnd -> card
        EmptyPassage -> card
        GuardedPassage monsters -> GuardedPassage $ L.delete monster monsters
        GuardRoom monsters -> GuardRoom $ L.delete monster monsters
        StorageRoom monsters -> StorageRoom $ L.delete monster monsters
        ArrowTrap -> ArrowTrap
        Chasm -> Chasm
        LockedDoor -> LockedDoor
        TreasureChamber -> TreasureChamber
        HiddenCorner -> HiddenCorner


-- | Weapon score indicates how good a given item is as a weapon.
-- | Bigger is better.
weaponScore :: Item -> Natural
weaponScore item =
    case item of
        Sword -> 4
        Helmet -> 0
        Armour -> 0
        Rope -> 0
        LockPick -> 0
        HealingPotion -> 0
        MagicCrown -> 0


-- | Armour score indicates how good a given item is in defense.
-- | Bigger is better.
armourScore :: Item -> Natural
armourScore item =
    case item of
        Sword -> 0
        Helmet -> 2
        Armour -> 4
        Rope -> 0
        LockPick -> 0
        HealingPotion -> 0
        MagicCrown -> 0


-- | Strength of monster indicates how dangerous they are in combat.
monsterStrength :: Monster -> Strength
monsterStrength monster =
    case monster of
        Goblin -> MkStrength 6
        Orc -> MkStrength 10


-- | Dexterity of monster indicates how hard it is to run away from them.
monsterDexterity :: Monster -> Dexterity
monsterDexterity monster =
    case monster of
        Goblin -> MkDexterity 12
        Orc -> MkDexterity 6


-- | Player tries to jump over a chasm. This is a test of dexterity.
-- | If player succeeds, they continue their journey. If they fail, they
-- | fall into chasm, take damage and continue their journey.
jumpOverChasm :: (RandomGen g) => Game -> Rand g (Game, [ChasmResult])
jumpOverChasm game = do
    roll <- dice 2
    let player = gamePlayer game
    -- Player rolls two dice, 8 is added and if result is less than their
    -- dexterity, they'll manage to jump over the chasm.
    -- Player with dexterity 10 or less will not be able to jump the chasm.
    if roll + 8 < unDexterity (playerDexterity player)
        then
            -- successful jump
            return ( game
                   , [JumpedOver]
                   )
        else do
            -- player falls and takes damage
            -- since we're dealing with naturals, we have to make sure not to try to go
            -- under zero. Doing that would cause runtime error being raised.
            let newHp = if unHP (playerHP player) >= 3
                then
                    MkHP $ unHP (playerHP player) - 3
                else
                    MkHP 0
            return ( game & gamePlayerL . playerHPL .~newHp
                   , [FellIntoChasm ]
                   )


-- | Player uses rope to cross the chasm safely. If they don't have rope
-- | they'll try to jump over instead.
ropeOverChasm :: (RandomGen g) => Game -> Rand g (Game, [ChasmResult])
ropeOverChasm game =
    if Rope `elem` playerGear (gamePlayer game)
        then
            return ( game & gamePlayerL . playerGearL %~ L.delete Rope
                   , [RopedOver]
                   )

        else do
            -- Player wants to rope over the chasm, but they don't have rope.
            -- This means there's a bug in the game. We report the bug and
            -- try to jump instead.
            (game', results) <- jumpOverChasm game

            return (game', TriedToRopeOverWithoutRope : results)


-- | Player takes a detour in order to avoid crossing a chasm. Three cards
-- | are suffled back into the deck from the discard pile.
goAroundChasm :: (RandomGen g) => Game -> Rand g (Game, [ChasmResult])
goAroundChasm game = do
    newDeck <- shuffle $ gameDeck game <> L.take 3 (gameDiscardDeck game)

    return ( game & gameDeckL .~ newDeck
                  & gameDiscardDeckL %~ L.drop 3
           , [WentAroundTheChasm])


-- | Player tries to break the door by kicking. This is test of strength.
-- | Successful check will break the door and failure will cause damage
-- | on the player.
breakDoor :: (RandomGen g) => Game -> Rand g (Game, [DoorResult])
breakDoor game = do
    roll <- dice 2
    let player = gamePlayer game
    -- Player rolls two dice, 8 is added and if result is less than their
    -- strength, they'll manage to kick the door down.
    -- Player with stregth 10 or less will not be able to break the door.
    if roll + 8 < unStrength (playerStrength player)
        then
            -- successful kick
            return ( game
                   , [BrokeTheDoor]
                   )
        else do
            -- the door resists and player takes damage
            -- since we're dealing with naturals, we have to make sure not to try to go
            -- under zero. Doing that would cause runtime error being raised.
            let newHp = if unHP (playerHP player) >= 2
                then
                    MkHP $ unHP (playerHP player) - 2
                else
                    MkHP 0
            return ( game & gamePlayerL . playerHPL .~newHp
                   , [HurtTheLeg]
                   )


-- | Player uses lock pick to force the door open. If they don't have lock pick
-- | they will try kicking instead.
lockPickDoor :: (RandomGen g) => Game -> Rand g (Game, [DoorResult])
lockPickDoor game = do
    if LockPick `elem` playerGear (gamePlayer game)
        then
            return ( game & gamePlayerL . playerGearL %~ L.delete LockPick 
                   , [LockPickedDoor]
                   )

        else do
            -- Player wants to use lock pick, but they don't have one.
            -- This means there's a bug in the game. We report the bug and
            -- try to kick the door instead.
            (game', results) <- breakDoor game

            return (game', TriedToLockPickWithoutTools : results)


-- | Player elects finding a different route. Up to three cards are shuffled
-- | into deck from discard pile.
goAroundDoor :: (RandomGen g) => Game -> Rand g (Game, [DoorResult])
goAroundDoor game = do
    newDeck <- shuffle $ gameDeck game <> L.take 3 (gameDiscardDeck game)

    return ( game & gameDeckL .~ newDeck
                  & gameDiscardDeckL %~ L.drop 3
           , [WentAroundTheDoor])


-- | Player uses an item.
useItem :: (RandomGen g) => Game -> Item -> Rand g (Game, [ItemResult])
useItem game HealingPotion = do
    if HealingPotion `elem` playerGear (gamePlayer game)
        then do
            let currentHp = unHP $ playerHP (gamePlayer game)
            let maxHp = unHP $ playerMaxHP (gamePlayer game)
            healing <- dice 2
            let newHP = if currentHp + healing <= maxHp
                then
                    MkHP $ currentHp + healing
                else
                    MkHP maxHp
            return ( game & gamePlayerL . playerHPL .~ newHP
                          & gamePlayerL . playerGearL %~ L.delete HealingPotion
                   , [PlayerHealed]
                   )
        else
            return (game, [TriedToUseNonexistentItem HealingPotion])

useItem game _ =
    return (game, [])
