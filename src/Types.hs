{-# LANGUAGE NoImplicitPrelude #-}

module Types where

import RIO

-- | Measure of physical prowess
newtype Strength = MkStrength { unStrength :: Natural }
  deriving (Show, Read, Eq)

-- | Measure of nibleness
newtype Dexterity = MkDexterity { unDexterity :: Natural }
  deriving (Show, Read, Eq)

-- | Measure of mental capabilities
newtype Mind = MkMind { unMind :: Natural }
  deriving (Show, Read, Eq)

-- | How far or close a person is to death
newtype HP = MkHP { unHP :: Natural }
  deriving (Show, Read, Eq)


-- | Player character
data Player = Player
  { playerStrength  :: !Strength 
  , playerDexterity :: !Dexterity
  , playerMind      :: !Mind
  , playerMaxHP     :: !HP
  , playerHP        :: !HP
  , playerGear      :: ![ Item ]
  } deriving ( Show, Read, Eq )

playerGearL :: Lens Player Player [Item] [Item]
playerGearL = lens playerGear (\x y -> x { playerGear = y })

playerHPL :: Lens Player Player HP HP
playerHPL = lens playerHP (\x y -> x { playerHP = y })

-- | Various monsters in the game
data Monster =
  Goblin
  | Orc
  deriving (Show, Read, Eq)


-- | Items that player can have
data Item =
  Sword
  | Helmet
  | Armour
  | Rope
  | LockPick
  | HealingPotion
  | MagicCrown
  deriving (Show, Read, Eq, Ord, Bounded, Enum)


-- | Various cards in the game
data Card =
  JourneyEnd
  | EmptyPassage
  | GuardedPassage [Monster]
  | GuardRoom [Monster]
  | StorageRoom [Monster]
  | ArrowTrap
  | Chasm
  | LockedDoor
  | TreasureChamber
  | HiddenCorner
  deriving (Show, Read, Eq)


-- | Current game
data Game = Game
  { gamePlayer      :: !Player
  , gameDeck        :: ![Card]
  , gameDiscardDeck :: ![Card]
  , gameCurrentCard :: !Card
  } deriving (Show, Read, Eq)

gamePlayerL :: Lens Game Game Player Player
gamePlayerL = lens gamePlayer (\x y -> x { gamePlayer = y })

gameDeckL :: Lens Game Game [Card] [Card]
gameDeckL = lens gameDeck (\x y -> x { gameDeck = y })

gameCurrentCardL :: Lens Game Game Card Card
gameCurrentCardL = lens gameCurrentCard (\x y -> x { gameCurrentCard = y })

gameDiscardDeckL :: Lens Game Game [Card] [Card]
gameDiscardDeckL = lens gameDiscardDeck (\x y -> x { gameDiscardDeck = y })


-- | Is game still running or not?
data GameCondition =
  GameRunning
  | GameEnded
  deriving (Show, Read, Eq)


-- | Choices player can make in main menu
data MainMenuChoice =
  StarNewGame
  | ExitGame
  deriving (Show, Read, Eq)


-- | Immediate results cards can have
data ImmediateResult =
  PlayerDied
  | CrownFound
  | DeckFinished
  | ItemsFound [Item]
  | NewCardRevealed Card
  | TrapAvoided
  | TrapTriggered Natural
  deriving (Show, Read, Eq)


data DiscardStatus =
  Discardable
  | Removable
  deriving (Show, Read, Eq)


cardMonsters :: Card -> [Monster]
cardMonsters card =
  case card of
    JourneyEnd -> []
    EmptyPassage -> []
    GuardedPassage monsters -> monsters
    GuardRoom monsters -> monsters
    StorageRoom monsters -> monsters
    ArrowTrap -> []
    Chasm -> []
    LockedDoor -> []
    TreasureChamber -> []
    HiddenCorner -> []

  
data CombatResult =
  PlayerAttackHits Monster
  | MonsterDies Monster
  | PlayerAttackMisses
  | MonsterAttackHits Monster
  | MonsterAttackMisses Monster
  | PlayerDies
  | CombatIsOver
  | CombatContinues
  | PlayerEscapes Monster
  | MonsterCatchesPlayer Monster
  deriving (Show, Read, Eq)


data ChasmResult =
    JumpedOver
    | RopedOver
    | TriedToRopeOverWithoutRope
    | FellIntoChasm
    | WentAroundTheChasm
    deriving (Show, Read, Eq)


data DoorResult =
  BrokeTheDoor
  | LockPickedDoor
  | TriedToLockPickWithoutTools
  | HurtTheLeg
  | WentAroundTheDoor
  deriving (Show, Read, Eq)


data ItemResult =
  PlayerHealed
  | TriedToUseNonexistentItem Item
  deriving (Show, Read, Eq)
