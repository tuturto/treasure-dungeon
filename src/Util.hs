{-# LANGUAGE NoImplicitPrelude #-}

-- | Silly utility module, used to demonstrate how to write a test
-- case.
module Util
  ( plus2, shuffle
  ) where

import RIO
import qualified RIO.List.Partial as L'
import qualified RIO.List as L
import Control.Monad.Random ( RandomGen, Rand, getRandomR )


plus2 :: Int -> Int
plus2 = (+ 2)

shuffle :: (RandomGen g, Eq a) => [a] -> Rand g [a]
shuffle [] = 
  return []

shuffle items = do
  let n = length items
  i <- getRandomR (0, n - 1)
  let item = items L'.!! i
  items' <- shuffle $ L.delete item items
  return $ item : items'
