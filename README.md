# treasure-dungeon

Treasure dungeon is a dungeon crawling adventure game. You're an adventurer
who seeks to explore an old and dangerous dungeon, with a goal of gathering as
much loot as possible. The deeper you go, the greater the rewards (and dangers) are.

## Build

* Run `stack build` to build the game
* You'll need development files for sdl2, sdl2-image, sdl2-ttf and sdl2-mixer

## Execute  

* Run `stack exec -- treasure-dungeon-text` to start the game in text mode
* Run `stack exec -- treasure-dungeon-gui` to start the game in graphical mode
* Run `stack exec -- treasure-dungeon-gui --verbose` to start the game with more logging.
* With `stack exec -- treasure-dungeon --help` you'll see help message

## Run tests

`stack test`
