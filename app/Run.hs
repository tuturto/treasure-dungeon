{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Run (run) where

import Import

import Control.Monad.Random ( evalRandIO )

import Rules ( rollNewCharacter, startGame )
import TextAppTypes
import UserInterface
    ( showMainMenu,
      displayNewCharacter,
      selectStartingGear,
      playGame,
      displayGameOver )


run :: RIO App ()
run = do
  choice <- showMainMenu
  case choice of
    StarNewGame -> do
      logDebug "New game starting..."
      logDebug "Rolling new character..."
      player <- liftIO $ evalRandIO rollNewCharacter
      displayNewCharacter player
      logDebug "Selecting starting gear..."
      gear <- selectStartingGear $ playerGear player
      logDebug "Preparing game..."
      game <- liftIO $ evalRandIO $ startGame player gear
      logDebug "Dealing first card..."
      finishedGame <- playGame game
      logDebug "Displaying game over..."
      displayGameOver finishedGame

    ExitGame ->
      return ()
