{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}

module UserInterface
  ( showMainMenu, displayNewCharacter, selectStartingGear, playGame
  , displayGameOver )
where

import Import
import Control.Monad.Random ( evalRandIO )
import qualified RIO.Text as T
import qualified RIO.List as L
import Prelude ( putStrLn, getLine )

import Rules
import TextAppTypes



-- | Display main menu
showMainMenu :: RIO App MainMenuChoice
showMainMenu = do
  logDebug "Displaying main menu"
  liftIO $ putStrLn "\n\n"
  liftIO $ putStrLn "Treasure Dungeon"
  liftIO $ putStrLn "****************"
  liftIO $ putStrLn ""
  liftIO $ putStrLn "1. Start a new game"
  liftIO $ putStrLn "2. Quit"

  mainMenuInput


mainMenuInput :: RIO App MainMenuChoice
mainMenuInput = do
    i <- liftIO getLine
    case i of
        "1" -> return StarNewGame
        "2" -> return ExitGame
        _ -> do
            logDebug $ displayShow $ "Incorrect menu choice: " <> i
            liftIO $ putStrLn "Please select 1 or 2"
            mainMenuInput


displayNewCharacter :: Player -> RIO App ()
displayNewCharacter player = do
    liftIO $ putStrLn "\n\n"
    liftIO $ putStrLn "Your character's stats"
    liftIO $ putStrLn $ "Strength: " <> show (unStrength $ playerStrength player)
    liftIO $ putStrLn $ "Dexterity: " <> show (unDexterity $ playerDexterity player)
    liftIO $ putStrLn $ "Mind: " <> show (unMind $ playerMind player)
    liftIO $ putStrLn $ "Hit points: " <> show (unHP $ playerHP player)

    return ()


selectStartingGear :: [ Item ] -> RIO App [ Item ]
selectStartingGear items = do
    liftIO $ putStrLn "\n\n"
    liftIO $ putStrLn "Your character's items:"
    liftIO $ mapM_ (\(MenuSelection i name _) ->
                            putStrLn $ show i <> ". " <> T.unpack name)
                       $ itemMenu items
    liftIO $ putStrLn "---------------"
    liftIO $ putStrLn "1. Add new item"
    liftIO $ putStrLn "2. Remove item"
    liftIO $ putStrLn "3. Start your adventure"

    startingGearInput items


startingGearInput :: [ Item ] -> RIO App [ Item ]
startingGearInput items = do
    i <- liftIO getLine
    case i of
        "1" -> do
            let startingItems = L.filter startingItem [minBound..]
            let availableItems = L.filter (\n -> not $ n `elem` items) startingItems
            let choices = itemMenu availableItems

            liftIO $ putStrLn "\n\n"
            liftIO $ putStrLn "Available items:"
            displayMenu choices
            liftIO $ putStrLn "Which one do you want to add (0 to cancel)?"
            itemChoice <- addItemInput choices
            let items' = case itemChoice of
                            Just newItem -> newItem : items
                            Nothing -> items
            selectStartingGear items'

        "2" -> do
            let choices = itemMenu items
            liftIO $ putStrLn "\n\n"
            liftIO $ putStrLn "Your character's items:"
            displayMenu choices
            liftIO $ putStrLn "Which one do you want to remove (0 to cancel)?"
            items' <- removeItemInput choices
            selectStartingGear items'

        "3" ->
            return items

        _ -> do
            logDebug $ displayShow $ "Incorrect menu choice: " <> i
            liftIO $ putStrLn "Please select 1, 2 or 3"
            startingGearInput items


addItemInput :: [ MenuSelection Item ] -> RIO App (Maybe Item)
addItemInput items = do
    i <- liftIO getLine
    if i /= "0"
        then do
            let match = L.find (\n -> show (menuSelectionIndex n) == i) items
            case match of
                Just m -> do
                    let (MenuSelection _ _ newItem) = m
                    return $ Just newItem

                Nothing -> do
                    liftIO $ putStrLn "Please select one of the items from list or 0 to cancel"
                    addItemInput items

        else
            return Nothing


removeItemInput :: [ MenuSelection Item ] -> RIO App [ Item ]
removeItemInput items = do
    i <- liftIO getLine
    if i /= "0"
        then do
            let match = L.find (\n -> show (menuSelectionIndex n) == i) items
            case match of
                Just m -> do
                    let (MenuSelection _ _ removed) = m
                    return $ L.delete removed $ menuSelectionObject <$> items

                Nothing -> do
                    liftIO $ putStrLn "Please select one of the items from list or 0 to cancel"
                    removeItemInput items

        else
            return $ menuSelectionObject <$> items


itemName :: Item -> T.Text
itemName item =
    case item of
        Sword -> "sword"
        Helmet -> "helmet"
        Armour -> "armour"
        Rope -> "rope"
        LockPick -> "lock pick"
        HealingPotion -> "healing potion"
        MagicCrown -> "magic crown"


monsterName :: Monster -> T.Text
monsterName monster =
    case monster of
        Goblin -> "goblin"
        Orc -> "orc"


data MenuSelection a where
    MenuSelection :: Eq a => Integer -> T.Text -> a -> MenuSelection a
deriving instance Eq a => Eq (MenuSelection a)


menuSelectionIndex :: MenuSelection a -> Integer
menuSelectionIndex (MenuSelection index _ _) =
    index


menuSelectionObject :: MenuSelection a -> a
menuSelectionObject (MenuSelection _ _ item) =
    item


itemMenu :: [Item] -> [MenuSelection Item]
itemMenu items =
    (\(i, item) -> MenuSelection i (itemName item) item) <$> L.zip [1..] items


monsterMenu :: [Monster] -> [MenuSelection Monster]
monsterMenu monsters =
    (\(i, monster) -> MenuSelection i (monsterName monster) monster) <$> L.zip [1..] monsters


playGame :: Game -> RIO App Game
playGame =
    doFlipCard >=> doResolveCard >=> doFindItems >=> doUseItems >=> doEndTurn


doFlipCard :: Game -> RIO App Game
doFlipCard game = do
    case checkForEnd game of
        GameRunning -> do
            liftIO $ putStrLn "\n\n"
            (game', results) <- liftIO $ evalRandIO $ flipCard game
            mapM_ displayResult results
            displayStats game'
            return game'

        GameEnded ->
            return game


doResolveCard :: Game -> RIO App Game
doResolveCard game = do
    case checkForEnd game of
        GameRunning ->
            resolveCard game

        GameEnded ->
            return game


doFindItems :: Game -> RIO App Game
doFindItems game = do
    case checkForEnd game of
        GameRunning -> do
            let items = roomItems game
            if not $ null items
                then do
                    liftIO $ putStrLn ("You find " <> T.unpack (neatList (itemName <$> items)) <> " here.")
                    let choices = itemMenu items
                    displayMenu choices
                    liftIO $ putStrLn "Which one do you want to take (0 to continue without taking anything)?"
                    i <- liftIO getLine

                    let match = L.find (\n -> show (menuSelectionIndex n) == i) choices
                    case match of
                        Just choice -> do
                            let (MenuSelection _ _ item) = choice
                            liftIO $ putStrLn ("You take " <> T.unpack (itemName item) <> " and continue forward.")
                            
                            return $ game & gamePlayerL . playerGearL %~ (item :)

                        Nothing ->
                            doFindItems game

                else
                    return game

        GameEnded ->
            return game


doUseItems :: Game -> RIO App Game
doUseItems game = do
    case checkForEnd game of
        GameRunning -> do
            let items = L.filter usableItem $ playerGear $ gamePlayer game

            if not $ null items
                then do
                    let choices = itemMenu items
                    liftIO $ putStrLn "\n"
                    displayMenu choices
                    liftIO $ putStrLn "You have a moment of peace. Do you want to use an item (0 to skip)?"
                    i <- liftIO getLine
                    if i /= "0"
                        then do
                            let match = L.find (\n -> show (menuSelectionIndex n) == i) choices
                            case match of
                                Just choice -> do
                                    let (MenuSelection _ _ item) = choice
                                    (game', results) <- liftIO $ evalRandIO $ useItem game item
                                    mapM_ displayItemResult results
                                    return game'
                                Nothing ->
                                    doUseItems game
                        else
                            return game
                else
                    return game

        GameEnded ->
            return game


doEndTurn :: Game -> RIO App Game
doEndTurn game = do
    case checkForEnd game of
        GameRunning -> do
            liftIO $ putStrLn "Press enter to draw next card"
            _ <- liftIO getLine
            playGame game

        GameEnded ->
            return game


displayItemResult :: ItemResult -> RIO App ()
displayItemResult result =
    case result of
        PlayerHealed -> 
            liftIO $ putStrLn "You feel a lot better"

        TriedToUseNonexistentItem item ->
            logWarn $ displayShow ("player tried to use nonexistent item: " <> itemName item)


-- | Display player stats
displayStats :: Game -> RIO App ()
displayStats game = do
    let player = gamePlayer game
    liftIO $ putStrLn ""
    liftIO $ putStrLn ("STR: " <> show (unStrength $ playerStrength player)
                        <> " DEX: " <> show (unDexterity $ playerDexterity player)
                        <> " Mind: " <> show (unMind $ playerMind player)
                        <> " HP: " <> show (unHP $ playerHP player)
                        <> " / " <> show (unHP $ playerMaxHP player))
    liftIO $ putStrLn ("Cards left: " <> show (L.length $ gameDeck game))


-- | Resolve card by displaying options and asking user input until card has
-- | been resolved and play can continue or the end condition has been reached.
resolveCard :: Game -> RIO App Game
resolveCard game@Game { gameCurrentCard = JourneyEnd } =
    return game

resolveCard game@Game { gameCurrentCard = EmptyPassage } =
    return game

resolveCard game@Game { gameCurrentCard = GuardedPassage [] } =
    return game

resolveCard game@Game { gameCurrentCard = GuardedPassage monsters } = do
    liftIO $ putStrLn "\n\n"
    liftIO $ putStrLn ("Room is guarded by " <> T.unpack (neatList $ monsterName <$> monsters))
    liftIO $ putStrLn "1. attack"
    liftIO $ putStrLn "2. try to run away"
    i <- liftIO getLine
    case i of
        "1" ->
            handleAttack game

        "2" ->
            handleRunAway game

        _ ->
            resolveCard game

resolveCard game@Game { gameCurrentCard = GuardRoom [] } =
    return game

resolveCard game@Game { gameCurrentCard = GuardRoom monsters } = do
    liftIO $ putStrLn "\n\n"
    liftIO $ putStrLn ("Room is guarded by " <> T.unpack (neatList $ monsterName <$> monsters))
    liftIO $ putStrLn "1. attack"
    liftIO $ putStrLn "2. try to run away"
    i <- liftIO getLine
    case i of
        "1" ->
            handleAttack game

        "2" ->
            handleRunAway game

        _ ->
            resolveCard game

resolveCard game@Game { gameCurrentCard = StorageRoom [] } =
    return game

resolveCard game@Game { gameCurrentCard = StorageRoom monsters } = do
    liftIO $ putStrLn "\n\n"
    liftIO $ putStrLn ("Room is guarded by " <> T.unpack (neatList $ monsterName <$> monsters))
    liftIO $ putStrLn "1. attack"
    liftIO $ putStrLn "2. try to run away"
    i <- liftIO getLine
    case i of
        "1" ->
            handleAttack game

        "2" ->
            handleRunAway game

        _ ->
            resolveCard game

resolveCard game@Game { gameCurrentCard = ArrowTrap } =
    return game

resolveCard game@Game { gameCurrentCard = Chasm } = do
    liftIO $ putStrLn "\n\n"
    liftIO $ putStrLn "What do you want to do?"
    let options = if Rope `elem` playerGear (gamePlayer game)
        then
            [ MenuSelection 1 "Jump over" JumpOverChasm
            , MenuSelection 2 "Use rope to cross" RopeOverChasm
            , MenuSelection 3 "Find another route" GoAroundChasm
            ]
        else
            [ MenuSelection 1 "Jump over" JumpOverChasm
            , MenuSelection 2 "Find another route" GoAroundChasm
            ]

    displayMenu options
    i <- liftIO getLine
    let match = L.find (\n -> show (menuSelectionIndex n) == i) options

    case match of
        Just choice -> do
            (game', results) <- case choice of
                (MenuSelection _ _ JumpOverChasm) ->
                    liftIO $ evalRandIO $ jumpOverChasm game

                (MenuSelection _ _ RopeOverChasm) ->
                    liftIO $ evalRandIO $ ropeOverChasm game

                (MenuSelection _ _ GoAroundChasm) ->
                    liftIO $ evalRandIO $ goAroundChasm game

            mapM_ displayChasmResult results
            return game'

        Nothing ->
            resolveCard game


resolveCard game@Game { gameCurrentCard = LockedDoor } = do
    liftIO $ putStrLn "\n\n"
    liftIO $ putStrLn "What do you want to do?"
    let options = if LockPick `elem` playerGear (gamePlayer game)
        then
            [ MenuSelection 1 "Break the door" BreakDoor
            , MenuSelection 2 "Use lock pick to open the door" LockPickDoor
            , MenuSelection 3 "Find another route" GoAroundDoor
            ]
        else
            [ MenuSelection 1 "Break the door" BreakDoor
            , MenuSelection 2 "Find another route" GoAroundDoor
            ]

    displayMenu options
    i <- liftIO getLine
    let match = L.find (\n -> show (menuSelectionIndex n) == i) options

    case match of
        Just choice -> do
            (game', results) <- case choice of
                (MenuSelection _ _ BreakDoor) ->
                    liftIO $ evalRandIO $ breakDoor game

                (MenuSelection _ _ LockPickDoor) ->
                    liftIO $ evalRandIO $ lockPickDoor game

                (MenuSelection _ _ GoAroundDoor) ->
                    liftIO $ evalRandIO $ goAroundDoor game

            mapM_ displayDoorResult results
            return game'

        Nothing ->
            resolveCard game


resolveCard game@Game { gameCurrentCard = TreasureChamber } =
    return game

resolveCard game@Game { gameCurrentCard = HiddenCorner } =
    return game


displayDoorResult :: DoorResult -> RIO App ()
displayDoorResult result =
    case result of
        BrokeTheDoor ->
            liftIO $ putStrLn "You broke the door in pieces"

        LockPickedDoor ->
            liftIO $ putStrLn "You forced the lock open"

        TriedToLockPickWithoutTools ->
            logWarn "Player tried to lockpick without tools"

        HurtTheLeg ->
            liftIO $ putStrLn "The door was sturdier than you thought and you hurt your leg"

        WentAroundTheDoor ->
            liftIO $ putStrLn "You decided to find another route"


displayChasmResult :: ChasmResult -> RIO App ()
displayChasmResult result =
    case result of
        JumpedOver ->
            liftIO $ putStrLn "You jumped over the chasm"

        RopedOver ->
            liftIO $ putStrLn "You rope over the chasm. Sadly you lose your rope"

        TriedToRopeOverWithoutRope ->
            logWarn "Player tried to cross chasm using non-existent rope"

        FellIntoChasm ->
            liftIO $ putStrLn "You fell into chasm and hurt yourself badly"

        WentAroundTheChasm ->
            liftIO $ putStrLn "You decided to find another route"


data ChasmOptions =
    JumpOverChasm
    | RopeOverChasm
    | GoAroundChasm
    deriving (Show, Read, Eq)


data DoorOptions =
    BreakDoor
    | LockPickDoor
    | GoAroundDoor
    deriving (Show, Read, Eq)


handleAttack :: Game -> RIO App Game
handleAttack game = do
    let menuItems = monsterMenu $ cardMonsters $ gameCurrentCard game
    liftIO $ putStrLn "Select who to attack"
    liftIO $ mapM_ (\(MenuSelection i name _) ->
                    putStrLn $ show i <> ". " <> T.unpack name)
                    menuItems
    i <- liftIO getLine
    let match = L.find (\n -> show (menuSelectionIndex n) == i) menuItems
    case match of
        Just m -> do
            let (MenuSelection _ _ monster) = m
            (game', results) <- liftIO $ evalRandIO $ attack game monster
            mapM_ displayCombatResult results
            if unHP (playerHP $ gamePlayer game') > 0 && not (null $ cardMonsters $ gameCurrentCard game')
                then
                    handleAttack game'

                else
                    return game'

        Nothing ->
            handleAttack game


handleRunAway :: Game -> RIO App Game
handleRunAway game = do
    (game', results) <- liftIO $ evalRandIO $ runAway game
    mapM_ displayCombatResult results

    if unHP (playerHP $ gamePlayer game') > 0 && not (null $ cardMonsters $ gameCurrentCard game')
        then do
            displayNewCard $ gameCurrentCard game'
            handleAttack game'

        else do
            displayNewCard $ gameCurrentCard game'
            return game'


displayCombatResult :: CombatResult -> RIO App ()
displayCombatResult result =
    case result of
        PlayerAttackHits monster ->
            liftIO $ putStrLn ("Your attack hits " <> T.unpack (monsterName monster))

        MonsterDies monster ->
            liftIO $ putStrLn (T.unpack (monsterName monster) <> " is dead!")

        PlayerAttackMisses ->
            liftIO $ putStrLn "Your attack misses"

        MonsterAttackHits monster ->
            liftIO $ putStrLn (T.unpack (monsterName monster) <> " hits you")

        MonsterAttackMisses monster ->
            liftIO $ putStrLn (T.unpack (monsterName monster) <> " fails to hit you")

        PlayerDies ->
            liftIO $ putStrLn "You die"

        CombatIsOver ->
            liftIO $ putStrLn "Combat is over"

        CombatContinues ->
            liftIO $ putStrLn "Combat continues"

        PlayerEscapes monster ->
            liftIO $ putStrLn ("You dodge the " <> T.unpack (monsterName monster))

        MonsterCatchesPlayer monster ->
            liftIO $ putStrLn (T.unpack (monsterName monster) <> " catches you!")


displayGameOver :: Game -> RIO App ()
displayGameOver game = do
    liftIO $ putStrLn "\n\n"
    if MagicCrown `elem` (playerGear . gamePlayer) game
        then
            liftIO $ putStrLn "You found the crown!"
        else
            liftIO $ putStrLn "You did not find the crown."
    liftIO $ putStrLn "Game Over"


-- | Display immediate effects of newly revealed card
displayResult :: ImmediateResult -> RIO App ()
displayResult result = do
    liftIO $ putStrLn ""
    case result of
        PlayerDied ->
            liftIO $ putStrLn "You have died."

        CrownFound ->
            liftIO $ putStrLn "You found the crown!"

        DeckFinished -> do
            logWarn "Deck has been exhausted and crown has not been found"
            liftIO $ putStrLn "You have searched the whole dungeon, yet have not found the crown."

        NewCardRevealed card ->
            displayNewCard card

        -- we don't want to notify player if they have discovered nothing at all.
        -- normally this should not happen, but let's play safe here.
        ItemsFound [] -> do
            logWarn "Empty list of items was discovered"
            return ()

        ItemsFound items -> do
            liftIO $ putStrLn ("You find " <> T.unpack (neatList $ itemName <$> items))
            return ()

        TrapAvoided ->
            liftIO $ putStrLn "You deftly avoid the trap"

        TrapTriggered damage -> do
            liftIO $ putStrLn "You didn't manage to dodge in time!"
            liftIO $ putStrLn ("You suffer " <> show damage <> " points of damage.")


-- | Build nice looking list of showable things
-- | []                            = ""
-- | ["apple"]                     = "apple"
-- | ["apple", "orange"]           = "apple and orange"
-- | ["apple", "orange", "banana"] = "apple, orange and banana"
neatList :: [Text] -> Text
neatList [] = ""
neatList [i] = i
neatList [i, j] = i `T.append` " and " `T.append` j
neatList (i : j@(_ : _)) = i `T.append` ", " `T.append` neatList j


-- | Display a menu with numbered entries
displayMenu :: [MenuSelection a] -> RIO App ()
displayMenu choices = do
    _ <- liftIO $ mapM (\(MenuSelection index name _) ->
                            putStrLn $ show index <> ". " <> T.unpack name)
                            choices
    return ()


-- | A new card has been revealed, display it to the player
displayNewCard :: Card -> RIO App ()
displayNewCard card =
    case card of
        JourneyEnd ->
            liftIO $ putStrLn "Your journey has come to end"

        EmptyPassage ->
            liftIO $ putStrLn "You sneak through an empty passage"

        GuardedPassage monsters ->
            case monsters of
                [] ->
                    liftIO $ putStrLn "You sneak through an empty passage"

                _ ->
                    liftIO $ putStrLn $ "This passage is guarded by "  <> (T.unpack . neatList $ monsterName <$> monsters)

        GuardRoom monsters ->
            case monsters of
                [] ->
                    liftIO $ putStrLn "Guard room is empty"

                _ ->
                    liftIO $ putStrLn ("This guard room is inhabitated by " <> (T.unpack . neatList $ monsterName <$> monsters))

        StorageRoom monsters ->
            case monsters of
                [] ->
                    liftIO $ putStrLn "Storage room is silent"

                _ ->
                    liftIO $ putStrLn $ "This storage room is guarded by "  <> (T.unpack . neatList $ monsterName <$> monsters)

        ArrowTrap ->
            liftIO $ putStrLn "There's an arrow trap here!"

        Chasm ->
            liftIO $ putStrLn "Wide chasm blocks your way"

        LockedDoor ->
            liftIO $ putStrLn "Locked blocks your way"

        TreasureChamber ->
            liftIO $ putStrLn "You have found the treasure chamber!"

        HiddenCorner ->
            liftIO $ putStrLn "You catch you breath in hidden corner"
