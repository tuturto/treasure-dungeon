{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}

module Main (main) where

import Import
import Run
import RIO.Process
import Options.Applicative.Simple
import qualified Paths_treasure_dungeon
import TextAppTypes

main :: IO ()
main = do
  (options, ()) <- simpleOptions
    $(simpleVersion Paths_treasure_dungeon.version)
    "Treasure Dungeon"
    "Dungeon adventure game, where your goal is to gather as much loot as you can."
    (Options
       <$> switch ( long "verbose"
                 <> short 'v'
                 <> help "Verbose output for debugging purposes"
                  )
    )
    empty
  lo <- logOptionsHandle stderr (optionsVerbose options)
  pc <- mkDefaultProcessContext
  withLogFunc lo $ \lf ->
    let app = App
          { appLogFunc = lf
          , appProcessContext = pc
          , appOptions = options
          }
     in runRIO app run
