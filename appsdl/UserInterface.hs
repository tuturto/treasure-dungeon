{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}

module UserInterface
  ( renderUI, update )
where

import Import
import Control.Monad.Random ( evalRandIO )
import qualified RIO.Text as T
import qualified RIO.List as L
import SdlAppTypes
import SDL
import SDL.Font as Font
import Foreign.C.Types (CInt(..))

import Rules


renderUI :: UIState -> RIO App ()
renderUI uiState = 
  case uiState of
    TitleScreen -> 
      renderTitleScreen

    MainMenu -> 
      renderMainMenu

    SelectingStartingGear player gear -> 
      renderSelectingStartingGear player gear

    ApplyingCard game results ->
      renderApplyingCard game results
        
    PreCombat game ->
      renderPreCombat game

    ResolvingCombat game results ->
      renderResolvingCombat game results
        
    ResolvingChasm game result ->
      renderResolvingChasm game result

    ResolvingDoor game result ->
      renderResolvingDoor game result

    LootingRoom game ->
      renderLootingRoom game

    UsingItems game ->
      renderUsingItems game

    DrawingCard game ->
      renderDrawingCard game

    GameFinished game ->
      renderGameFinished game

    GameExiting ->
      return ()


update :: [Event] -> UIState -> RIO App UIState
update events uiState
  | quitRequested events = do
      logInfo "Quit requested"
      return GameExiting

  | otherwise =
      case uiState of
        TitleScreen -> 
          updateTitleScreen events uiState

        MainMenu -> 
          updateMainMenu events uiState

        SelectingStartingGear player gear -> 
          updateSelectingStartingGear events player gear

        ApplyingCard game results ->
          updateApplyingCard events game results
                
        PreCombat game ->
          updatePreCombat events game
                
        ResolvingCombat game results->
          updateResolvingCombat events game results

        ResolvingChasm game result ->
          updateResolvingChasm events game result

        ResolvingDoor game result ->
          updateResolvingDoor events game result

        LootingRoom game ->
          updateLootingRoom events game

        UsingItems game ->
          updateUsingItems events game

        DrawingCard game ->
          updateDrawingCard events game

        GameFinished game ->
          updateGameFinished events game

        GameExiting ->
          return uiState


quitRequested :: [Event] -> Bool
quitRequested events =
  let quitEvent event =
        case eventPayload event of
          WindowClosedEvent _ ->
            True

          QuitEvent  ->
            True

          _ -> False
  in
    any quitEvent events


pointInsideRectangele :: Rectangle CInt -> Point V2 CInt -> Bool
pointInsideRectangele (Rectangle (P (V2 x y)) (V2 w h)) (P (V2 px py)) =
  px >= x 
    && px <= x + w 
    && py >= y 
    && py <= y + h


rectangleClicked :: Rectangle CInt -> Event -> Bool
rectangleClicked rectangle event =
  case eventPayload event of
        MouseButtonEvent mouseButtonEvent ->
          mouseButtonEventMotion mouseButtonEvent == Pressed
            && pointInsideRectangele rectangle (P (V2 (fromIntegral x) (fromIntegral y)))
          where
            (P (V2 x y)) = mouseButtonEventPos mouseButtonEvent

        _ -> False


renderTitleScreen :: RIO App ()
renderTitleScreen = do
  renderer <- asks appRenderer
  mainImage <- asks (texturesTitleScreen . appTextures)
  clear renderer
  copy renderer mainImage Nothing Nothing
  present renderer


updateTitleScreen :: [Event] -> UIState -> RIO App UIState
updateTitleScreen events uiState = do
  let continueEvent event =
        case eventPayload event of
              KeyboardEvent keyboardEvent ->
                keyboardEventKeyMotion keyboardEvent == Pressed

              MouseButtonEvent mouseButtonEvent ->
                mouseButtonEventMotion mouseButtonEvent == Pressed

              _ -> False        

  if any continueEvent events
    then
      return MainMenu
    else
      return uiState


renderMainMenu :: RIO App ()
renderMainMenu = do
  renderer <- asks appRenderer
  mainImage <- asks (texturesTitleScreen . appTextures)
  menuImage <- asks (texturesMainMenu . appTextures)
  clear renderer
  copy renderer mainImage Nothing Nothing
  copy renderer menuImage Nothing (Just (Rectangle (P $ V2 111 123) (V2 103 51)))
  present renderer


updateMainMenu :: [Event] -> UIState -> RIO App UIState
updateMainMenu events uiState
  -- start button
  | any (rectangleClicked (Rectangle (P (V2 114 126)) (V2 96 20))) events = do
      logInfo "Rolling a new character"
      player <- liftIO $ evalRandIO rollNewCharacter
      return $ SelectingStartingGear player []

  -- quit button
  | any (rectangleClicked (Rectangle (P (V2 114 150)) (V2 96 20))) events = return GameExiting

  | otherwise = return uiState


renderText :: Point V2 CInt -> (V4 Word8) -> Text -> RIO App ()
renderText location colour text = do
  font <- asks appFont
  renderer <- asks appRenderer   

  surface <- Font.solid font colour text
  texture <- createTextureFromSurface renderer surface

  info <- queryTexture texture
  let w = textureWidth info
  let h = textureHeight info

  copy renderer texture Nothing (Just (Rectangle location (V2 w h)))

  freeSurface surface
  destroyTexture texture

  return ()


renderList :: Point V2 CInt -> (V4 Word8) -> [Text] -> RIO App ()
renderList (P (V2 x y)) colour texts = do
  let ys = [ y, (y + 9).. ]
  let points = fmap (\n -> P (V2 x n)) ys
  
  mapM_ (\(p, t) -> renderText p colour t) (zip points texts)


deckTexture :: [Card] -> RIO App (Rectangle CInt)
deckTexture cards
  | length cards > 20 = do 
      return $ Rectangle (P (V2 0 0)) (V2 65 85)
  | length cards > 15 = do
      return $ Rectangle (P (V2 65 0)) (V2 65 85)
  | length cards > 10 = do
      return $ Rectangle (P (V2 130 0)) (V2 65 85)
  | length cards > 5 = do
      return $ Rectangle (P (V2 195 0)) (V2 65 85)
  | otherwise = do
      return $ Rectangle (P (V2 260 0)) (V2 65 85)


renderDeck :: [Card] -> RIO App ()
renderDeck cards = do
  renderer <- asks appRenderer
  deck <- asks (texturesDeck . appTextures)
  rect <- deckTexture cards
  copy renderer 
       deck
       (Just rect)
       (Just (Rectangle (P (V2 245 7)) (V2 65 85)))


-- | Render stat block at the bottom of the screen.
-- | Doesn't clear renderer.
renderStats :: Player -> RIO App ()
renderStats player = do
  let dex = show $ unDexterity $ playerDexterity player
  let strength = show $ unStrength $ playerStrength player
  let mind = show $ unMind $ playerMind player
  let hp = show $ unHP $ playerHP player
  let maxHp = show $ unHP $ playerMaxHP player

  ltGrey <- asks (colourLtGrey . appColours)

  renderText (P (V2 8 187)) ltGrey (T.pack $ "Str: " <> strength)
  renderText (P (V2 58 187)) ltGrey (T.pack $ "Dex: " <> dex)
  renderText (P (V2 108 187)) ltGrey (T.pack $ "Mind: " <> mind)
  renderText (P (V2 158 187)) ltGrey (T.pack $ "HP: " <> hp <> "/" <> maxHp)


-- locations of buttons
button_0 :: Num a => Rectangle a
button_0 = Rectangle (P (V2 286 189)) (V2 13 9)
button_1 :: Num a => Rectangle a
button_1 = Rectangle (P (V2 304 189)) (V2 13 9)


-- locations of cards
card_0 :: Num a => Rectangle a
card_0 = Rectangle (P (V2 8 12)) (V2 60 80)
card_1 :: Num a => Rectangle a
card_1 = Rectangle (P (V2 78 12)) (V2 60 80)
card_2 :: Num a => Rectangle a
card_2 = Rectangle (P (V2 148 12)) (V2 60 80)
card_3 :: Num a => Rectangle a
card_3 = Rectangle (P (V2 8 98)) (V2 60 80)
card_4 :: Num a => Rectangle a
card_4 = Rectangle (P (V2 78 98)) (V2 60 80)
card_5 :: Num a => Rectangle a
card_5 = Rectangle (P (V2 148 98)) (V2 60 80)
card_6 :: Num a => Rectangle a
card_6 = Rectangle (P (V2 245 98)) (V2 60 80)


startingGearLayout :: [(Item, Rectangle CInt)]
startingGearLayout =
  [ ( Sword,    card_0 )
  , ( Helmet,   card_1 )
  , ( Armour,   card_2 )
  , ( Rope,     card_3 )
  , ( LockPick, card_4 )
  ]


renderSelectingStartingGear :: Player -> [Item] -> RIO App ()
renderSelectingStartingGear player gear = do
  renderer <- asks appRenderer
  bg <- asks (texturesPlayArea . appTextures)
  items <- asks (texturesItems . appTextures)
  selector <- asks (texturesSelector . appTextures)

  let drawItemCard = \(i, (item, location)) -> do
        copy renderer
             items 
             (Just (Rectangle (P (V2 (60 * i) 0)) (V2 60 80)))
             (Just location)
        when (item `elem` gear)
             (copy renderer
                   selector
                   Nothing
                   (Just location))
          

  clear renderer
  copy renderer bg Nothing Nothing
  mapM_ drawItemCard $ zip [0..] startingGearLayout
  renderDeck startingDeck

  renderBackButton
  renderContinueButton

  ltGrey <- asks (colourLtGrey . appColours)
  renderList (P (V2 220 95)) ltGrey [ "Select up to two", "cards.", "Click continue", "when you're ready."]

  renderStats player
  present renderer


-- | Find out, if any cards of given layout have been clicked using supplied list of events.
-- | First match will be returned
cardClicked :: [(card, Rectangle CInt)] -> [Event] -> Maybe card
cardClicked layout events = 
  listToMaybe $ mapMaybe cards events
  where
    cards event = listToMaybe $ mapMaybe (\(item, rect) -> if rectangleClicked rect event
                                                                then Just item
                                                                else Nothing) 
                                         layout


addRemoveItem :: [Item] -> Item -> [Item]
addRemoveItem items item
  | item `elem` items = 
      L.delete item items

  | item `notElem` items && length items < 2 =
      item : items

  | otherwise =
      items


updateSelectingStartingGear :: [Event] -> Player -> [Item] -> RIO App UIState
updateSelectingStartingGear events player gear
  -- back button
  | any (rectangleClicked button_0) events = do
      return MainMenu

  -- continue button
  | any (rectangleClicked button_1) events = do
      logInfo "New game starting"
      game <- liftIO $ evalRandIO $ startGame player gear
      logInfo "Revealing the first card"
      (game', results) <- liftIO $ evalRandIO $ flipCard game
      return $ ApplyingCard game' results
      
  -- item
  | otherwise =
      return $ maybe (SelectingStartingGear player gear)
                     (\item -> SelectingStartingGear player $ addRemoveItem gear item)
                     (cardClicked startingGearLayout events)


-- | Index of card in the texture sheet
cardIndex :: Card -> CInt
cardIndex card =
  case card of
    JourneyEnd       -> 0
    EmptyPassage     -> 1
    GuardedPassage _ -> 2
    GuardRoom _      -> 3
    StorageRoom _    -> 4
    ArrowTrap        -> 5
    Chasm            -> 6
    LockedDoor       -> 7
    TreasureChamber  -> 8
    HiddenCorner     -> 9


-- | Render card right side up on the left side of the deck
renderCurrentCard :: Card -> RIO App ()
renderCurrentCard card = do
  renderer <- asks appRenderer
  cards <- asks (texturesLocations . appTextures)
  let i = cardIndex card
  
  copy renderer 
       cards
       (Just (Rectangle (P (V2 (60 * i) 0)) (V2 60 80)))
       (Just card_6)


renderContinueButton :: RIO App ()
renderContinueButton = do
  buttons <- asks (texturesButtons . appTextures)
  renderer <- asks appRenderer
  copy renderer
       buttons 
       (Just (Rectangle (P (V2 0 0)) (V2 13 9)))
       (Just button_1)


renderBackButton :: RIO App ()
renderBackButton = do
  buttons <- asks (texturesButtons . appTextures)
  renderer <- asks appRenderer
  copy renderer 
       buttons 
       (Just (Rectangle (P (V2 13 0)) (V2 13 9)))
       (Just button_0)


renderApplyingCard :: Game -> [ImmediateResult] -> RIO App ()
renderApplyingCard game results = do
  renderer <- asks appRenderer

  clear renderer
  renderActivePlay game

  -- arrow card has something extra to render
  when (gameCurrentCard game == ArrowTrap)
    (do
      ltGrey <- asks (colourLtGrey . appColours)
      let res = L.find (\n -> case n of 
                                TrapTriggered _ -> True
                                _ -> False) results
      let description = case res of
                          Just (TrapTriggered damage) ->
                            ("You got hit for " `T.append` (T.pack $ show damage) `T.append` " points of damage!")

                          Just _ ->
                            ""

                          Nothing ->
                            "You dodged the arrow"          

      if (unHP $ playerHP $ gamePlayer game) > 0
        then
          renderList (P (V2 8 12)) ltGrey [ description, "Click continue to proceed" ]
        else
          renderList (P (V2 8 12)) ltGrey [ description, "You have died!", "Click continue to view the end." ]

      renderContinueButton)

  -- display continue button in treasure chamber
  when (gameCurrentCard game == TreasureChamber)
    renderContinueButton

  present renderer


updateApplyingCard :: [Event] -> Game -> [ImmediateResult] -> RIO App UIState
updateApplyingCard events game results =
  case checkForEnd game of
    GameEnded ->
      if any (rectangleClicked button_1) events
        then
          return $ GameFinished game
        else
          return $ ApplyingCard game results

    GameRunning ->
      if gameCurrentCard game == ArrowTrap 
        then
          if any (rectangleClicked button_1) events
              && checkForEnd game == GameRunning
            then
              completeApplyingPhase game
            else
              return $ ApplyingCard game results
        else
          completeApplyingPhase game


completeApplyingPhase :: Game -> RIO App UIState
completeApplyingPhase game =
  case gameCurrentCard game of
    Chasm -> 
      return $ ResolvingChasm game []

    LockedDoor ->
      return $ ResolvingDoor game []

    _ ->
      if (not . null . cardMonsters . gameCurrentCard) game
        then
          return $ PreCombat game
        else
          return $ LootingRoom game


renderMonster :: (Monster, Rectangle CInt) -> RIO App ()
renderMonster (monster, target) = do
  texture <- asks (texturesMonsters . appTextures)
  renderer <- asks appRenderer
  let source = case monster of
                  Goblin ->
                    Rectangle (P (V2 60 0)) (V2 60 80)

                  Orc ->
                    Rectangle (P (V2 0 0)) (V2 60 80)

  copy renderer texture (Just source) (Just target)


renderMonsters :: [Monster] -> RIO App ()
renderMonsters monsters = do
  mapM_ renderMonster $ zip monsters [ card_0, card_1, card_2 ]


renderPreCombat :: Game -> RIO App ()
renderPreCombat game = do
  renderer <- asks appRenderer
  options <- asks (texturesOptions . appTextures)

  clear renderer
  renderActivePlay game

  renderMonsters $ cardMonsters $ gameCurrentCard game

  copy renderer
       options
       (Just (Rectangle (P (V2 0 0)) (V2 60 80)))
       (Just card_3)
  copy renderer
       options
       (Just (Rectangle (P (V2 60 0)) (V2 60 80)))
       (Just card_4)

  present renderer


escaped :: [CombatResult] -> Bool
escaped results =
  not (any (\x -> case x of
                    MonsterCatchesPlayer _ ->
                      True

                    _ ->
                      False)
           results)


updatePreCombat :: [Event] -> Game -> RIO App UIState
updatePreCombat events game
  -- run
  | any (rectangleClicked card_3) events = do
      (game', res) <- liftIO $ evalRandIO $ runAway game
      if escaped res
        then
          return $ LootingRoom game'
        else
          return $ ResolvingCombat game' res

  -- fight
  | any (rectangleClicked card_4) events =
    return $ ResolvingCombat game []

  | otherwise = return $ PreCombat game


monsterName :: Monster -> T.Text
monsterName monster =
  case monster of
    Goblin -> 
      "goblin"

    Orc -> 
      "orc"


displayCombatResult :: CombatResult -> Maybe Text
displayCombatResult result =
  case result of
    PlayerAttackHits monster ->
      Just $ T.append "Your attack hits " (monsterName monster)

    MonsterDies monster ->
      Just $ T.append (monsterName monster) " dies!"

    PlayerAttackMisses ->
      Just "Your attack misses"

    MonsterAttackHits monster ->
      Just $ T.append (monsterName monster) " hits you!"

    MonsterAttackMisses monster ->
      Just $ T.append (monsterName monster) " misses"

    PlayerDies ->
      Just "You die"

    CombatIsOver ->
      Nothing

    CombatContinues ->
      Just "Combat continues"

    PlayerEscapes monster ->
      Just $ T.append "You dodge the " (monsterName monster)

    MonsterCatchesPlayer monster ->
      Just $ T.append (monsterName monster) " blocks you!"


renderResolvingCombat :: Game -> [CombatResult] -> RIO App ()
renderResolvingCombat game results = do
  renderer <- asks appRenderer
  ltGrey <- asks (colourLtGrey . appColours)

  clear renderer
  renderActivePlay game

  let descriptions = mapMaybe displayCombatResult results

  renderMonsters $ cardMonsters $ gameCurrentCard game

  renderList (P (V2 8 98)) ltGrey descriptions

  when (null (cardMonsters $ gameCurrentCard game))
    (do
      renderList (P (V2 8 98)) ltGrey (descriptions <> [ "Click continue to proceed." ])
      renderContinueButton)

  when (checkForEnd game == GameEnded)
    renderContinueButton

  present renderer


updateResolvingCombat :: [Event] -> Game -> [CombatResult] -> RIO App UIState
updateResolvingCombat events game results =
  case checkForEnd game of
    GameEnded ->
      if any (rectangleClicked button_1) events
        then
          return $ GameFinished game
        else
          return $ ResolvingCombat game results

    GameRunning ->
      case card of
        Nothing ->
          if null (cardMonsters $ gameCurrentCard game)
              && any (rectangleClicked button_1) events
            then
              return $ LootingRoom game
            else      
              return $ ResolvingCombat game results
            
        Just monster -> do
          (game', newResults) <- liftIO $ evalRandIO $ attack game monster
          return $ ResolvingCombat game' newResults

      where
        layout = zip (cardMonsters $ gameCurrentCard game) [ card_0, card_1, card_2 ]
        card = cardClicked layout events


chasmLayout :: Num a => Game -> [(ChasmOptions, Rectangle a)]
chasmLayout game =
  zip options [ card_3, card_4, card_5 ]
  where
    options = 
      if Rope `elem` playerGear (gamePlayer game)
        then
          [ JumpOverChasm
          , RopeOverChasm
          , GoAroundChasm
          ]

        else
          [ JumpOverChasm
          , GoAroundChasm
          ]


renderChasmOption :: (ChasmOptions, Rectangle CInt) -> RIO App ()
renderChasmOption (option, target) = do
  texture <- asks (texturesOptions . appTextures)
  renderer <- asks appRenderer
  let source = case option of
                  JumpOverChasm ->
                    Rectangle (P (V2 120 0)) (V2 60 80)

                  RopeOverChasm ->
                    Rectangle (P (V2 180 0)) (V2 60 80)

                  GoAroundChasm ->
                    Rectangle (P (V2 240 0)) (V2 60 80)
  
  copy renderer texture (Just source) (Just target)


displayChasmResult :: ChasmResult -> Maybe Text
displayChasmResult result =
  case result of
    JumpedOver ->
      Just "You jump over the chasm."

    RopedOver ->
      Just "You use your rope to cross the chasm."
    
    TriedToRopeOverWithoutRope ->
      Nothing

    FellIntoChasm ->
      Just "You fall into the chasm."

    WentAroundTheChasm ->
      Just "You decide to find another route."


renderResolvingChasm :: Game -> [ChasmResult] -> RIO App ()
renderResolvingChasm game result = do
  renderer <- asks appRenderer

  clear renderer
  renderActivePlay game

  if not (null result)
    then
      do
        let descriptions = mapMaybe displayChasmResult result
        ltGrey <- asks (colourLtGrey . appColours)
        renderList (P (V2 8 12)) ltGrey (descriptions <> ["Click continue to proceed"])
        renderContinueButton
    else
      mapM_ renderChasmOption (chasmLayout game)

  present renderer


updateResolvingChasm :: [Event] -> Game -> [ChasmResult] -> RIO App UIState
updateResolvingChasm events game result = do
  case checkForEnd game of
    GameEnded ->
      if any (rectangleClicked button_1) events
        then
          return $ GameFinished game
        else
          return $ ResolvingChasm game result
    
    GameRunning ->
      case card of
        Just JumpOverChasm -> do
          (game', res) <- liftIO $ evalRandIO $ jumpOverChasm game
          return $ ResolvingChasm game' res
          
        Just RopeOverChasm -> do
          (game', res) <- liftIO $ evalRandIO $ ropeOverChasm game
          return $ ResolvingChasm game' res

        Just GoAroundChasm -> do
          (game', res) <- liftIO $ evalRandIO $ goAroundChasm game
          return $ ResolvingChasm game' res

        Nothing ->
          if not (null result) && any (rectangleClicked button_1) events
            then
              return $ LootingRoom game
            else
              return $ ResolvingChasm game result
      where
        card = cardClicked (chasmLayout game) events


doorLayout :: Num a => Game -> [(DoorOptions, Rectangle a)]
doorLayout game =
  zip options [ card_3, card_4, card_5 ]
  where
    options = 
      if LockPick `elem` playerGear (gamePlayer game)
        then
          [ BreakDoor
          , LockPickDoor
          , GoAroundDoor
          ]

        else
          [ BreakDoor
          , GoAroundDoor
          ]


renderDoorOption :: (DoorOptions, Rectangle CInt) -> RIO App ()
renderDoorOption (option, target) = do
  texture <- asks (texturesOptions . appTextures)
  renderer <- asks appRenderer
  let source = case option of
                  BreakDoor ->
                    Rectangle (P (V2 300 0)) (V2 60 80)

                  LockPickDoor ->
                    Rectangle (P (V2 360 0)) (V2 60 80)

                  GoAroundDoor ->
                    Rectangle (P (V2 240 0)) (V2 60 80)
  
  copy renderer texture (Just source) (Just target)


displayDoorResult :: DoorResult -> Maybe Text
displayDoorResult res =
  case res of
    BrokeTheDoor ->
      Just "You break the door into tiny pieces!"

    LockPickedDoor ->
      Just "You open the lock with your tools"

    TriedToLockPickWithoutTools ->
      Nothing

    HurtTheLeg ->
      Just "You kick the door and hurt your leg"

    WentAroundTheDoor ->
      Just "You decide to find another route"


renderResolvingDoor :: Game -> [DoorResult] -> RIO App ()
renderResolvingDoor game result = do  
  renderer <- asks appRenderer    

  clear renderer
  renderActivePlay game

  if not (null result)
    then
      do
        let descriptions = mapMaybe displayDoorResult result
        ltGrey <- asks (colourLtGrey . appColours)
        renderList (P (V2 8 12)) ltGrey (descriptions <> ["Click continue to proceed"])
        renderContinueButton
    else
      mapM_ renderDoorOption (doorLayout game)

  present renderer


updateResolvingDoor :: [Event] -> Game -> [DoorResult] -> RIO App UIState
updateResolvingDoor events game result =
  case checkForEnd game of
    GameEnded ->
      if any (rectangleClicked button_1) events
        then 
          return $ GameFinished game
        else
          return $ ResolvingDoor game result
    
    GameRunning ->
      case cardClicked (doorLayout game) events of
        Just BreakDoor -> do
          (game', res) <- liftIO $ evalRandIO $ breakDoor game
          return $ ResolvingDoor game' res
          
        Just LockPickDoor -> do
          (game', res) <- liftIO $ evalRandIO $ lockPickDoor game
          return $ ResolvingDoor game' res

        Just GoAroundDoor -> do
          (game', res) <- liftIO $ evalRandIO $ goAroundDoor game
          return $ ResolvingDoor game' res

        Nothing ->
          if not (null result) && any (rectangleClicked button_1) events
            then
              return $ LootingRoom game
            else
              return $ ResolvingDoor game result


-- | Rectagle of given item in items texture.
-- | This function is used to get which portion of item texture contains
-- | texture for given item.
itemRect :: Item -> Rectangle CInt 
itemRect item =
  case item of
    Sword -> 
      Rectangle (P (V2 0 0)) (V2 60 80)
    Helmet -> 
      Rectangle (P (V2 60 0)) (V2 60 80)
    Armour ->
      Rectangle (P (V2 120 0)) (V2 60 80)
    Rope ->
      Rectangle (P (V2 180 0)) (V2 60 80)
    LockPick ->
      Rectangle (P (V2 240 0)) (V2 60 80)
    HealingPotion ->
      Rectangle (P (V2 300 0)) (V2 60 80)
    MagicCrown ->
      Rectangle (P (V2 360 0)) (V2 60 80)


renderLootingRoom :: Game -> RIO App ()
renderLootingRoom game = do
  renderer <- asks appRenderer
  itemTexture <- asks (texturesItems . appTextures)

  clear renderer
  renderActivePlay game

  let items = roomItems game
  let drawItemCard = \(item, location) -> do
        copy renderer
             itemTexture 
             (Just $ itemRect item)
             (Just location)
  let itemLayout = zip items [ card_3, card_4, card_5 ]

  when (not $ null items)
    (do
      mapM_ drawItemCard itemLayout
      ltGrey <- asks (colourLtGrey . appColours)
      renderList (P (V2 8 12)) ltGrey [ "Select an item to take with you."
                                      , "Or click continue to proceed without taking one."
                                      ]
      renderContinueButton)
  
  present renderer


updateLootingRoom :: [Event] -> Game -> RIO App UIState
updateLootingRoom events game
  | null items =
      return $ UsingItems game
  -- continue button
  | any (rectangleClicked button_1) events = do
      return $ UsingItems game

  -- item card
  | isJust item =
      -- card clicked -> take item
      return $ maybe (UsingItems game)
                (\x -> UsingItems $ game & gamePlayerL . playerGearL %~ (x :))
                item

  | otherwise =
      return $ LootingRoom game
  where
    item = cardClicked (zip items [ card_3, card_4, card_5 ]) events
    items = roomItems game


renderUsingItems :: Game -> RIO App ()
renderUsingItems game = do
  renderer <- asks appRenderer
  items <- asks (texturesItems . appTextures)

  clear renderer
  renderActivePlay game

  when (HealingPotion `elem` (playerGear $ gamePlayer game))
    (do
      ltGrey <- asks (colourLtGrey . appColours)
      renderList (P (V2 8 12)) ltGrey [ "You can use your items now."
                                      , "Or click continue to proceed."
                                      ]
      copy renderer
           items
           (Just (Rectangle (P (V2 300 0)) (V2 60 80)))
           (Just card_3)
      renderContinueButton)

  present renderer


updateUsingItems :: [Event] -> Game -> RIO App UIState
updateUsingItems events game
  -- No healing potion -> move to next phase
  | not (HealingPotion `elem` (playerGear $ gamePlayer game)) =
      return $ DrawingCard game

  -- continue clicked -> move to next phase
  | any (rectangleClicked button_1) events = do
      return $ DrawingCard game

  -- potion clicked -> use it
  | any (rectangleClicked card_3) events = do
      -- hard coding healing potion here, since it's the only usable item
      (game', _) <- liftIO $ evalRandIO $ useItem game HealingPotion
      return $ DrawingCard game'

  | otherwise =
      return $ UsingItems game
    

renderDrawingCard :: Game -> RIO App ()
renderDrawingCard game = do
  renderer <- asks appRenderer
  ltGrey <- asks (colourLtGrey . appColours)

  clear renderer
  renderActivePlay game

  renderText (P (V2 8 12)) ltGrey "Click continue to draw next card"

  renderContinueButton
  present renderer


updateDrawingCard :: [Event] -> Game -> RIO App UIState
updateDrawingCard events game
  -- continue clicked -> move to next phase
  | any (rectangleClicked button_1) events = do
      (game', results) <- liftIO $ evalRandIO $ flipCard game
      return $ ApplyingCard game' results

  | otherwise =
      return $ DrawingCard game


renderGameFinished :: Game -> RIO App ()
renderGameFinished game = do
  renderer <- asks appRenderer
  ltGrey <- asks (colourLtGrey . appColours)
  texture <- asks (texturesEndings . appTextures)

  clear renderer
  renderActivePlay game

  if MagicCrown `elem` (playerGear $ gamePlayer game)
    then do
      renderList (P (V2 8 12)) ltGrey [ "You have conquered the dungeon and found the"
                                      , "crown!. You feel elated as you hold the crown"
                                      , "on your hands."
                                      , "It's time to return to home."
                                      ]
      copy renderer
           texture
           (Just (Rectangle (P (V2 0 0)) (V2 60 80)))
           (Just card_4)
    else do
      renderList (P (V2 8 12)) ltGrey [ "Treasure dungeon has bestet you."
                                      , "Your vision slowly fades into black as you lie"
                                      , "on the stone floor."
                                      , "Your time has come."
                                      ]
      copy renderer
           texture
           (Just (Rectangle (P (V2 60 0)) (V2 60 80)))
           (Just card_4)

  renderContinueButton
  present renderer


updateGameFinished :: [Event] -> Game -> RIO App UIState
updateGameFinished events game
  -- continue clicked -> move to main menu
  | any (rectangleClicked button_1) events = do
      return $ MainMenu

  | otherwise =
      return $ GameFinished game


-- | renders playing area, completed with deck, current card and
-- | player stats. Does not clear the renderer.
renderActivePlay :: Game -> RIO App ()
renderActivePlay game = do
  let player = gamePlayer game
  renderer <- asks appRenderer
  bg <- asks (texturesPlayArea . appTextures)

  copy renderer bg Nothing Nothing

  renderDeck $ gameDeck game
  renderCurrentCard $ gameCurrentCard game
  
  renderStats player


data ChasmOptions =
  JumpOverChasm
  | RopeOverChasm
  | GoAroundChasm
  deriving (Show, Read, Eq)


data DoorOptions =
  BreakDoor
  | LockPickDoor
  | GoAroundDoor
  deriving (Show, Read, Eq)
