{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Main (main) where

import Import
import Run
import RIO.Process
import Options.Applicative.Simple
import qualified Paths_treasure_dungeon
import SdlAppTypes

import SDL
import qualified SDL.Image as Image
import qualified SDL.Font as Font


main :: IO ()
main = do
  (options, ()) <- simpleOptions
    $(simpleVersion Paths_treasure_dungeon.version)
    "Treasure Dungeon"
    "Dungeon adventure game, where your goal is to gather as much loot as you can."
    (Options
       <$> switch ( long "verbose"
                 <> short 'v'
                 <> help "Verbose output for debugging purposes"
                  )
    )
    empty

  bracket loadResources
          freeResources
          (runApplication options)
  

runApplication :: Options -> (Textures, Font.Font, Renderer, Window) -> IO ()
runApplication options (textures, font, renderer, _) = do
  lo <- logOptionsHandle stderr (optionsVerbose options)
  pc <- mkDefaultProcessContext
  withLogFunc lo $ \lf ->
    let app = App
          { appLogFunc = lf
          , appProcessContext = pc
          , appOptions = options
          , appRenderer = renderer
          , appTextures = textures
          , appFont = font
          , appColours = colours
          }
     in runRIO app run


-- | Free textures and fonts used by program
freeResources :: (Textures, Font.Font, Renderer, Window) -> IO ()
freeResources (textures, font, _, window) = do
  mapM_ (\n -> destroyTexture $ n textures)
        [ texturesTitleScreen
        , texturesMainMenu
        , texturesItems
        , texturesPlayArea
        , texturesDeck
        , texturesSelector
        , texturesLocations
        , texturesButtons
        , texturesMonsters
        , texturesOptions
        , texturesEndings
        ]

  Font.free font
  destroyWindow window

  Font.quit
  Image.quit
  SDL.quit


-- | Display a loading screen and load images and fonts from disk
loadResources :: IO (Textures, Font.Font, Renderer, Window)
loadResources = do
  SDL.initializeAll
  Image.initialize [Image.InitPNG]
  Font.initialize

  window <- createWindow "Treasure Dungeon" (defaultWindow { windowInitialSize = V2 1280 800})
  renderer <- createRenderer window (-1) defaultRenderer
  rendererLogicalSize renderer $= Just (V2 320 200)

  font <- Font.load "art/Pixel64_v1.2.ttf" 15
  
  clear renderer
  rendererDrawColor renderer $= V4 0 0 0 255
  loadingSurface <- Font.solid font (V4 250 250 250 255) "Loading..."
  loading <- createTextureFromSurface renderer loadingSurface

  loadingInfo <- queryTexture loading
  let w = textureWidth loadingInfo
  let h = textureHeight loadingInfo

  copy renderer loading Nothing (Just (Rectangle (P $ V2 (160 - (w `div` 2)) (100 - (w `div` 2))) (V2 w h)))
  present renderer

  titleScreenTexture <- Image.loadTexture renderer "art/title.png"
  mainMenuTexture <- Image.loadTexture renderer "art/menu.png"
  itemsTexture <- Image.loadTexture renderer "art/items.png"
  playAreaTexture <- Image.loadTexture renderer "art/playarea.png"
  deckTexture <- Image.loadTexture renderer "art/deck.png"
  selectorTexture <- Image.loadTexture renderer "art/selector.png"
  locationsTexture <- Image.loadTexture renderer "art/locations.png"
  buttonsTexture <- Image.loadTexture renderer "art/buttons.png"
  monstersTexture <- Image.loadTexture renderer "art/monsters.png"
  optionsTexture <- Image.loadTexture renderer "art/options.png"
  endingsTexture <- Image.loadTexture renderer "art/endings.png"

  freeSurface loadingSurface
  destroyTexture loading

  return $ ( Textures { texturesTitleScreen = titleScreenTexture 
                      , texturesMainMenu = mainMenuTexture
                      , texturesItems = itemsTexture
                      , texturesPlayArea = playAreaTexture
                      , texturesDeck = deckTexture
                      , texturesSelector = selectorTexture
                      , texturesLocations = locationsTexture
                      , texturesButtons = buttonsTexture
                      , texturesMonsters = monstersTexture
                      , texturesOptions = optionsTexture
                      , texturesEndings = endingsTexture
                      }
           , font
           , renderer
           , window )
