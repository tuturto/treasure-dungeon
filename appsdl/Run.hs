{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Run (run) where

import Import
import SdlAppTypes
import SDL
import UserInterface ( renderUI, update )


run :: RIO App ()
run = do
  logInfo "Entering main loop"
  _ <- appLoop TitleScreen
  logInfo "Main loop finished"


appLoop :: UIState -> RIO App UIState
appLoop uiState = do
  renderUI uiState
  events <- pollEvents
  uiState' <- update events uiState

  case uiState' of
    GameExiting -> return uiState'
    _ -> appLoop uiState'
