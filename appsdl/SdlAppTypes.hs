{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module SdlAppTypes where

import RIO
import RIO.Process

import SDL
import qualified SDL.Font as Font
import Types

-- | Command line arguments
data Options = Options
  { optionsVerbose :: !Bool
  }

data App = App
  { appLogFunc  :: !LogFunc
  , appProcessContext :: !ProcessContext
  , appOptions  :: !Options
  -- Add other app-specific configuration information here
  , appTextures :: !Textures
  , appRenderer :: !Renderer
  , appFont     :: !Font.Font
  , appColours  :: !Colours
  }

instance HasLogFunc App where
  logFuncL = lens appLogFunc (\x y -> x { appLogFunc = y })
instance HasProcessContext App where
  processContextL = lens appProcessContext (\x y -> x { appProcessContext = y })


data Textures = Textures
  { texturesTitleScreen :: !Texture
  , texturesMainMenu    :: !Texture
  , texturesItems       :: !Texture
  , texturesPlayArea    :: !Texture
  , texturesDeck        :: !Texture
  , texturesSelector    :: !Texture
  , texturesLocations   :: !Texture
  , texturesButtons     :: !Texture
  , texturesMonsters    :: !Texture
  , texturesOptions     :: !Texture
  , texturesEndings     :: !Texture
  }


data Colours = Colours
  { colourBlack   :: !(V4 Word8)
  , colourWhite   :: !(V4 Word8)
  , colourRed     :: !(V4 Word8)
  , colourCyan    :: !(V4 Word8)
  , colourPurple  :: !(V4 Word8)
  , colourGreen   :: !(V4 Word8)
  , colourBlue    :: !(V4 Word8)
  , colourYellow  :: !(V4 Word8)
  , colourOrange  :: !(V4 Word8)
  , colourBrown   :: !(V4 Word8)
  , colourPink    :: !(V4 Word8)
  , colourDkGrey  :: !(V4 Word8)
  , colourMGrey   :: !(V4 Word8)
  , colourLtGreen :: !(V4 Word8)
  , colourLtBlue  :: !(V4 Word8)
  , colourLtGrey  :: !(V4 Word8)
  }


colours :: Colours
colours = Colours
  { colourBlack   = V4 0x0 0x0 0x0 0xff
  , colourWhite   = V4 0xff 0xff 0xff 0xff
  , colourRed     = V4 0x68 0x37 0x2b 0xff
  , colourCyan    = V4 0x70 0xa4 0xb2 0xff
  , colourPurple  = V4 0x6f 0x3d 0x86 0xff
  , colourGreen   = V4 0x58 0x8d 0x43 0xff
  , colourBlue    = V4 0x35 0x28 0x79 0xff
  , colourYellow  = V4 0xb8 0xc7 0x6f 0xff
  , colourOrange  = V4 0x6f 0x4f 0x25 0xff
  , colourBrown   = V4 0x43 0x39 0x00 0xff
  , colourPink    = V4 0x9a 0x67 0x59 0xff
  , colourDkGrey  = V4 0x44 0x44 0x44 0xff
  , colourMGrey   = V4 0x6c 0x6c 0x6c 0xff
  , colourLtGreen = V4 0x9a 0xd2 0x84 0xff
  , colourLtBlue  = V4 0x6c 0x5e 0xb5 0xff
  , colourLtGrey  = V4 0x95 0x95 0x95 0xff
  }


data UIState =
  TitleScreen
  | MainMenu
  | SelectingStartingGear Player [Item]
  | ApplyingCard Game [ImmediateResult]
  | PreCombat Game
  | ResolvingCombat Game [CombatResult]
  | ResolvingChasm Game [ChasmResult]
  | ResolvingDoor Game [DoorResult]
  | LootingRoom Game
  | UsingItems Game
  | DrawingCard Game
  | GameFinished Game
  | GameExiting
  deriving (Show, Read, Eq)
